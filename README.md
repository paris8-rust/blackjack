les règles: https://www.casinoonlinefrancais.info/blackjack/regles-du-jeu.html

Objectifs:

1. Pouvoir lancer une partie de Blackjack respectant les règles de celui-ci (1 joueur vs 1 machine) et annonçant la victoire ou défaite du joueur (non-graphique)

2. Créer un système d'argent virtuel pour pouvoir choisir une mise initiale à chaque partie

3. Introduire plus de joueurs

4. Faire en sorte qu'un assistant virtuel donne à chaque coup des conseils pour le coup suivant (pourcentage de chance de ne pas perdre)

Bonus:
Utiliser Amethyst pour travailler le design du jeu (en-dehors du terminale)